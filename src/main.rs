pub mod models;
pub mod schema;

use byteorder::LE;

use clap::{Command, CommandFactory, Parser, Subcommand, ValueHint};
use clap_complete::{generate, Generator, Shell};
use diesel::prelude::*;
use diesel::Connection;
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
use dirs::data_dir;
use event_listener::Event;
use models::{NewTimer, Timer, TimerLocalOffset};
use notify_rust::{Notification, Timeout};
use parse_duration::parse;
use std::fs::create_dir_all;
use std::io;
use std::time::Duration;
use time::OffsetDateTime;
use zbus::zvariant::{from_slice, to_bytes, EncodingContext};
use zbus::Result as zbus_Result;
use zbus::{dbus_interface, dbus_proxy, Connection as zbus_Connection, ConnectionBuilder};

struct TimerService {
    done: Event,
}

pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!();

fn establish_connection() -> SqliteConnection {
    let path = data_dir().expect("No data dir");
    let app_path = path.join("timers-rs");
    create_dir_all(&app_path)
        .unwrap_or_else(|_| panic!("Error creating dir {}", app_path.display()));
    let database_url = format!("sqlite://{}", app_path.join("timers.db").to_str().unwrap());

    let mut connection = SqliteConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url));

    connection
        .run_pending_migrations(MIGRATIONS)
        .expect("Not able to run migrations");

    connection
}

#[dbus_interface(name = "org.anriha.timer")]
impl TimerService {
    async fn get_timers(&self) -> Vec<u8> {
        let ctxt = EncodingContext::<LE>::new_gvariant(0);

        to_bytes(ctxt, &read_timers().expect("Failed to read timers")).unwrap()
    }

    async fn create_timer(&self, duration: String, description: String) {
        match save_timer(duration, description) {
            Ok(_) => (),
            Err(e) => println!("{}", e),
        }
    }

    async fn done(&self) {
        self.done.notify(1);
    }
}

#[dbus_proxy(
    interface = "org.anriha.timer",
    default_service = "org.anriha.timer",
    default_path = "/org/anriha/timer"
)]
trait Timers {
    async fn get_timers(&self) -> zbus_Result<Vec<u8>>;
    async fn create_timer(&self, duration: String, description: String) -> zbus_Result<()>;
    async fn done(&self) -> zbus_Result<()>;
}

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Option<Commands>,
    #[arg(long = "generate", value_enum)]
    generator: Option<Shell>,
}

#[derive(Subcommand, Debug, PartialEq)]
enum Commands {
    List {},
    Create {
        #[arg(short='t', long, value_hint = ValueHint::Other)]
        duration: String,
        #[arg(short='d', long, value_hint = ValueHint::Other)]
        description: String,
    },
    Daemon {},
}

fn print_completions<G: Generator>(gen: G, cmd: &mut Command) {
    generate(gen, cmd, cmd.get_name().to_string(), &mut io::stdout());
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let cli = Cli::parse();

    if let Some(generator) = cli.generator {
        let mut cmd = Cli::command();
        print_completions(generator, &mut cmd);
    }

    match &cli.command {
        Some(Commands::List {}) => {
            let ctxt = EncodingContext::<LE>::new_gvariant(0);
            let connection = zbus_Connection::session().await?;
            let proxy = TimersProxy::new(&connection).await?;
            let reply = proxy.get_timers().await?;
            let timers: Vec<Timer> = from_slice(&reply, ctxt).unwrap();
            let timers: Vec<TimerLocalOffset> = timers.iter().map(|x| x.into()).collect();
            println!("{:?}", timers);
        }
        Some(Commands::Create {
            duration,
            description,
        }) => {
            let connection = zbus_Connection::session().await?;
            let proxy = TimersProxy::new(&connection).await?;
            proxy
                .create_timer(duration.to_string(), description.to_string())
                .await?;
        }
        Some(Commands::Daemon {}) => {
            start_daemon().await?;
        }
        None => {}
    }
    Ok(())
}

async fn start_daemon() -> anyhow::Result<()> {
    let timer = TimerService {
        done: event_listener::Event::new(),
    };
    let done_listener = timer.done.listen();
    let _ = ConnectionBuilder::session()?
        .name("org.anriha.timer")?
        .serve_at("/org/anriha/timer", timer)?
        .build()
        .await?;

    let _ = tokio::task::spawn_blocking(|| {
        check_timers().unwrap();
    });

    done_listener.wait();

    Ok(())
}

fn check_timers() -> anyhow::Result<()> {
    loop {
        let active_timers = read_timers()?;
        let now = OffsetDateTime::now_utc();
        for timer in active_timers {
            if (timer.started.unix_timestamp() as i128) + (timer.duration as i128)
                < now.unix_timestamp() as i128
            {
                let success = send_notification(&timer);
                if success {
                    save_timer_end(&timer)?;
                } else {
                    println!(
                        "Unable to send notification, notification daemon is probably not running!"
                    );
                }
            }
        }
        std::thread::sleep(Duration::from_secs(1));
    }
}

fn save_timer(duration: String, description: String) -> anyhow::Result<()> {
    use self::schema::timers::dsl::timers;
    let connection = &mut establish_connection();
    let now = OffsetDateTime::now_utc();
    let duration_parsed = parse(&duration)?;
    let duration = duration_parsed.as_secs() as i64;
    let new_timer = NewTimer {
        duration: &duration,
        description: &description,
        started: &now,
    };

    diesel::insert_into(timers)
        .values(&new_timer)
        .execute(connection)
        .expect("Error saving timer");

    Ok(())
}

fn save_timer_end(timer: &Timer) -> anyhow::Result<()> {
    use self::schema::timers::dsl::*;
    let connection = &mut establish_connection();
    let now = OffsetDateTime::now_utc();

    diesel::update(timers.find(timer.id))
        .set(ended.eq(now))
        .execute(connection)
        .expect("Error updating timer");

    Ok(())
}

fn read_timers() -> anyhow::Result<Vec<Timer>> {
    use self::schema::timers::dsl::*;
    let connection = &mut establish_connection();

    let active_timers = timers
        .filter(ended.is_null())
        .load::<Timer>(connection)
        .expect("Error loading timers");
    Ok(active_timers)
}

fn send_notification(timer: &Timer) -> bool {
    Notification::new()
        .summary("Timer")
        .body(&timer.description)
        .timeout(Timeout::Never)
        .show()
        .is_ok()
}

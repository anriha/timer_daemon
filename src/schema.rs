// @generated automatically by Diesel CLI.

diesel::table! {
    timers (id) {
        id -> Integer,
        duration -> BigInt,
        description -> Text,
        started -> TimestamptzSqlite,
        ended -> Nullable<TimestamptzSqlite>,
    }
}

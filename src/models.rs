use super::schema::timers;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use time::{ext::NumericalDuration, Duration, OffsetDateTime, UtcOffset};
use zbus::zvariant::Type;

#[derive(Serialize, Deserialize, Debug, Type, Queryable)]
pub struct Timer {
    pub id: i32,
    pub duration: i64,
    pub description: String,
    pub started: OffsetDateTime,
    pub ended: Option<OffsetDateTime>,
}

#[derive(Insertable)]
#[diesel(table_name = timers)]
pub struct NewTimer<'a> {
    pub duration: &'a i64,
    pub description: &'a str,
    pub started: &'a OffsetDateTime,
}

#[derive(Debug)]
pub struct TimerLocalOffset {
    pub id: i32,
    pub duration: Duration,
    pub description: String,
    pub started: OffsetDateTime,
    pub will_end: OffsetDateTime,
}

impl From<&Timer> for TimerLocalOffset {
    fn from(value: &Timer) -> Self {
        let local_offset = UtcOffset::from_hms(2, 0, 0).unwrap();
        TimerLocalOffset {
            id: value.id,
            duration: value.duration.seconds(),
            description: value.description.clone(),
            started: value.started.to_offset(local_offset),
            will_end: value.started.to_offset(local_offset) + value.duration.seconds(),
        }
    }
}

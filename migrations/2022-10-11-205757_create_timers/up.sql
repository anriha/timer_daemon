-- Your SQL goes here
CREATE TABLE IF NOT EXISTS timers
(
    id INTEGER PRIMARY KEY NOT NULL,
    duration BIGINT NOT NULL,
    description TEXT NOT NULL,
    started DATETIME NOT NULL,
    ended DATETIME
);
